const Article = require("../models/post");


exports.createArticle = (req,res) => {
    const articleObject = req.body.article;
    //supprime l'id qui sera recréé par la base de données
    delete articleObject._id;
    delete articleObject.userId;
    const article = new Article({
        ...articleObject,
        userId: req.auth.userId,
    });
    article.save()
        .then((savedArticle) => res.status(201).json({ message: 'Objet enregistré !', article: savedArticle }))
        .catch(error => res.status(500).json({error}));
};

exports.modifyArticle = (req,res)=>{
    Article.updateOne( {_id: req.params.id }, { $set: req.body })
        .then(()=> res.status(200).json({message:'Objet modifié !'}))
        .catch(error => res.status(500).json({error}));
};

exports.deleteArticle = (req, res) => {

    Article.deleteOne({ _id: req.params.id })
        .then(() => res.status(200).json({ message: 'Objet supprimé !'}))
        .catch(error => res.status(500).json({ error }));
};

exports.getOneArticle = (req, res) => {
    //récupère un article avec l'id spécifié
    Article.findOne({_id: req.params.id})
        .then(article =>res.status(200).json(article))
        .catch(error =>res.status(404).json({error}));
};

exports.getAllArticles =(req,res)=>{
    console.log("here")
    Article.find()
        .then(Articles => res.status(200).json(Articles))
        .catch(error => res.status(400).json({error}));
};