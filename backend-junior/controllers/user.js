// bcrypt est un package qui permet de hacher les mots de passe
const bcrypt = require('bcrypt');
// jsonwebtoken permet de créer des tokens et de les identifier
const jwt = require('jsonwebtoken');
const User = require('../models/User');


exports.signup = (req, res) => {
    bcrypt.hash(req.body.password, 10)  //On fait 10 tours de hachage qui permet une sécurité suffisante
        .then(hash => {
            // Enregistrement du nouvel utilisateur
            const user = new User({
                email: req.body.email,
                password: hash
            });
            // Enregistrement dans la base de données
            user.save()
                // 201 : pour une création de ressource
                .then((savedUser) => res.status(201).json({ message: 'Utilisateur créé !',userId: savedUser._id }))
                // 400 : erreur client
                .catch(error => res.status(400).json({ error }));
        })
        // 500 : erreur serveur
        .catch(error => res.status(500).json({ error }));
};

exports.login = (req, res) => {
    // Filtres les emails de la base de données
    User.findOne({ email: req.body.email })
        .then(user => {
            if (!user) {
                // Ne pas indiquer quelle entrée est fausse (RGPD)
                return res.status(401).json({ error: 'Utilisateur ou mot de passe incorrect !' });
            }
            bcrypt.compare(req.body.password, user.password)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({ error: 'Utilisateur ou mot de passe incorrect !' });
                    }
                    // le token permet d'authentifier l'utilisateur à chaque requête
                    res.status(200).json({
                        userId: user._id,
                        token: jwt.sign(
                            { userId: user._id },
                            'RANDOM_TOKEN_SECRET', // Clé secrète à modifier lors de la mise en production
                            { expiresIn: '24h' }
                        )
                    });
                })
                .catch(error => res.status(500).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
};

exports.modifyUser = (req, res) => {
    // filtre les id dans la base de données afin d'éviter a un autre utilisateur de modifier les données
    User.updateOne({ _id: req.params.id }, { $set: req.body })
        .then(() => res.status(200).json({ message: 'Utilisateur modifié !' }))
        .catch(error => res.status(400).json({ error }));
};
exports.deleteUser = (req, res) => {
    User.deleteOne({ _id: req.params.id })
        .then(() => res.status(200).json({ message: 'Utilisateur supprimé !'}))
        .catch(error => res.status(400).json({ error }));
};

exports.getOneUser = (req, res) => {
    User.findOne({_id: req.params.id})
        .then(user =>res.status(200).json(user))
        .catch(error =>res.status(404).json({error}));
};
