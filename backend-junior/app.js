const express =require ('express');
const app = express();
//on va utiliser mongoose afin de simplifier les interactions avec la base de données
const mongoose = require('mongoose');

//récupération des routes
const userRoutes = require('./routes/user');
const postRoutes = require('./routes/Post');


//connection à la base de données
mongoose.connect("mongodb://localhost:27017/mydbPrevimeteo")
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));


app.use(express.json());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

//enregistrement des collections
app.use('/user', userRoutes);
app.use('/article', postRoutes);
module.exports =app;