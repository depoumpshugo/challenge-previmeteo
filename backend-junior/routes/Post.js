const express =require('express');
const auth = require('../middleware/auth');
const articleCtrl = require('../controllers/post');
const router = express.Router();

//création des routes /article/
router.post('/', auth, articleCtrl.createArticle);
router.get('/', articleCtrl.getAllArticles);
router.get('/:id', articleCtrl.getOneArticle);
router.put('/:id', auth, articleCtrl.modifyArticle);
router.delete('/:id', auth, articleCtrl.deleteArticle);



module.exports = router;