const express = require('express');
const userCtrl= require('../controllers/user');
const sauceCtrl = require("../controllers/post");
const auth = require("../middleware/auth");
const router = express.Router();

//création des routes /user/
router.post('/signup',userCtrl.signup);
router.post('/login',userCtrl.login);

router.put('/:id', auth, userCtrl.modifyUser);
router.get('/:id', userCtrl.getOneUser);
router.delete('/:id', auth, userCtrl.deleteUser);


module.exports = router ;