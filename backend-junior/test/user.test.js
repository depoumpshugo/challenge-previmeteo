const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const jwt = require("jsonwebtoken");
const should = chai.should();

chai.use(chaiHttp);

describe('Authentification', () => {
    let createdUserId;
    let authToken;

    beforeEach(() => {
        const payload = { userId: createdUserId };
        authToken = jwt.sign(payload, 'RANDOM_TOKEN_SECRET');
    });

    describe('POST /user/signup', () => {
        it('devrait créer un nouvel utilisateur', (done) => {
            chai.request(server)
                .post('/user/signup')
                .send({ email: 'test@example.com', password: 'password123' })
                .end((err, res) => {
                    console.log("res.body :",res.body);
                    createdUserId= res.body.userId;
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Utilisateur créé !');
                    done();
                });
        });
    });

    describe('POST /user/login', () => {
        it('devrait connecter un utilisateur existant', (done) => {
            chai.request(server)
                .post('/user/login')
                .send({ email: 'test@example.com', password: 'password123' })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('userId');
                    res.body.should.have.property('token');
                    done();
                });
        });

        it('ne devrait pas connecter un utilisateur avec des informations incorrectes', (done) => {
            chai.request(server)
                .post('/user/login')
                .send({ email: 'test@example.com', password: 'motdepasseincorrect' })
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    res.body.should.have.property('error').eql('Utilisateur ou mot de passe incorrect !');
                    done();
                });
        });
    });
    describe('GET /user/:id', () => {
        it('devrait récupérer un utilisateur par son ID', (done) => {
            chai.request(server)
                .get(`/user/${createdUserId}`)
                .end((err, res) => {

                    console.log("res.body : ",res.body)
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });
    describe('PUT /user/:id', () => {
        it('devrait modifier un utilisateur existant', (done) => {
            chai.request(server)
                .put(`/user/${createdUserId}`)
                .set('Authorization', `Bearer ${authToken}`)
                .send({ email: 'modified@example.com', password: 'newpassword123' })
                .end((err, res) => {
                    console.log(createdUserId)
                    console.log(res.body);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Utilisateur modifié !');
                    done();
                });
        });
        it('ne devrait pas modifier un utilisateur existant', (done) => {
            chai.request(server)
                .put(`/user/mauvaisIdAModifier`)
                .set('Authorization', `Bearer ${authToken}`)
                .send({ email: 'modified@example.com', password: 'newpassword123' })
                .end((err, res) => {
                    console.log(res.body);
                    res.should.have.status(400);
                    done();
                });
        });
    });
    describe('DELETE /user/:id', () => {
        it('devrait supprimer un article existant', (done) => {
            chai.request(server)
                .delete(`/user/${createdUserId}`)
                .set('Authorization', `Bearer ${authToken}`) // Remplacez par un vrai jeton JWT si votre route nécessite une authentification
                .end((err, res) => {
                    console.log("res.body : ",res.body)
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Utilisateur supprimé !');
                    done();
                });
        });
        it('ne devrait pas supprimer un utilisateur existant', (done) => {
            chai.request(server)
                .put(`/user/mauvaisIdAModifier`)
                .set('Authorization', `Bearer ${authToken}`)
                .end((err, res) => {
                    console.log(res.body);
                    res.should.have.status(400);
                    done();
                });
        });
    });
});