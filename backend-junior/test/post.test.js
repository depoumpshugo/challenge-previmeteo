const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const jwt = require('jsonwebtoken');
const server = require('../app');


chai.use(chaiHttp);

describe('Articles', () => {
    let createdArticleId;
    let authToken;

    beforeEach(() => {
        const payload = { userId: '123456' };
        authToken = jwt.sign(payload, 'RANDOM_TOKEN_SECRET');
    });

    describe('POST /article/', () => {
        it('devrait créer un nouvel article', (done) => {
            chai.request(server)
                .post('/article')
                .set('Authorization', `Bearer ${authToken}`)
                .send({
                    article: {
                        name: 'Article de test',
                        description: 'Description de l\'article de test',
                        price: 19.99,
                    },
                })
                .end((err, res) => {
                    console.log("res.body : ",res.body);
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Objet enregistré !');
                    createdArticleId = res.body.article._id;
                    done();
                });
        });
    });

    describe('GET /article/', () => {
        it('devrait récupérer tous les articles', (done) => {
            chai.request(server)
                .get('/article')
                .end((err, res) => {
                    console.log("res.body : ",res.body)
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });

    describe('GET /article/:id', () => {
        it('devrait récupérer un article par son ID', (done) => {
            chai.request(server)
                .get(`/article/${createdArticleId}`)
                .end((err, res) => {
                    console.log("res.body : ",res.body)
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });

    describe('PUT /article/:id', () => {
        it('devrait modifier un article existant', (done) => {
            chai.request(server)
                .put(`/article/${createdArticleId}`)
                .set('Authorization', `Bearer ${authToken}`) // Remplacez par un vrai jeton JWT si votre route nécessite une authentification
                .send({
                    name: 'Nouveau nom de l\'article',
                    description: 'Nouvelle description de l\'article',
                    price: 29.99,
                })
                .end((err, res) => {
                    console.log("res.body : ",res.body)
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Objet modifié !');
                    done();
                });
        });
    });

    describe('DELETE /article/:id', () => {
        it('devrait supprimer un article existant', (done) => {
            chai.request(server)
                .delete(`/article/${createdArticleId}`)
                .set('Authorization', `Bearer ${authToken}`) // Remplacez par un vrai jeton JWT si votre route nécessite une authentification
                .end((err, res) => {
                    console.log("res.body : ",res.body)
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Objet supprimé !');
                    done();
                });
        });
    });
});