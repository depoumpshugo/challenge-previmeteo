# Évaluation des Compétences Backend Web - Développeur Junior

Bienvenue dans le processus d'évaluation des compétences pour le poste de développeur junior en backend web. Cette évaluation vise à évaluer les compétences techniques et la compréhension du candidat dans le domaine du développement backend.

## Prérequis

Assurez-vous d'avoir les éléments suivants installés sur votre machine avant de commencer l'évaluation :

- [ ] [Node.js](https://nodejs.org/)
- [ ] [npm](https://www.npmjs.com/) - gestionnaire de paquets pour Node.js
- [ ] [MongoDB](https://www.mongodb.com/) - base de données NoSQL

## Instructions

1. Clonez ce dépôt sur votre machine.
   ```bash
   git clone https://gitlab.com/previmeteo/challenges/backend-junior
   ```

2. Installez les dépendances nécessaires.
   ```bash
   cd evaluation-backend-junior
   npm install
   ```

3. Configurez votre base de données MongoDB. Assurez-vous que MongoDB est en cours d'exécution sur votre machine. Vous pouvez modifier la configuration de la base de données dans le fichier `config/database.js`.

4. Implémentez les fonctionnalités demandées en suivant les commentaires dans les fichiers de code fournis.

5. Testez vos implémentations avec les commandes appropriées.
   ```bash
   npm test
   ```

6. Assurez-vous que votre code respecte les bonnes pratiques, notamment en termes de lisibilité, de modularité et de gestion des erreurs.

7. Soumettez vos modifications en créant une branche avec votre nom.
   ```bash
   git checkout -b votre-nom
   git add .
   git commit -m "Ajout de mes implémentations"
   git push origin votre-nom
   ```

8. Créez une pull request depuis votre branche vers la branche principale.

## Tâches

Vous trouverez plusieurs fichiers dans ce dépôt. Chaque fichier contient des commentaires indiquant les fonctionnalités que vous devez implémenter. Assurez-vous de lire attentivement les commentaires et de suivre les instructions.

1. **auth.js** - Mettez en place un système d'authentification simple. Utilisez le framework de votre choix (Express, Koa, etc.).

2. **routes/user.js** - Implémentez les routes nécessaires pour créer, lire, mettre à jour et supprimer un utilisateur.

3. **routes/Post.js** - Implémentez les routes nécessaires pour créer, lire, mettre à jour et supprimer un article.

4. **test/user.test.js** et **test/post.test.js** - Ajoutez des tests unitaires pour les fonctionnalités que vous avez implémentées.

## Évaluation

Votre code sera évalué en fonction des critères suivants :

- Respect des consignes et des commentaires fournis.
- Qualité et lisibilité du code.
- Gestion des erreurs et des cas limites.
- Pertinence des tests unitaires.

N'hésitez pas à ajouter des commentaires dans votre code pour expliquer vos choix de conception et de mise en œuvre.

Bonne chance !
