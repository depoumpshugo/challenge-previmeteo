# Frontend Developper Challenge

Monté une webapp VueJs avec : 

- 1 Carte
- 1 Interface utilisateur
- 2 layer
 - TileLayer
 - Markers / LayerGroup

## Pré-requis

 - VueJS
 - leaflet
 - rainviewer
 - assets/datas.json

## Le test

Vous devez créer une webapp avec une carte sur laquelle vous afficherez un Layer type TileLayer activable/desactivable avec Rainviewer

Ensuite il vous faudra ajouté des markers en ce basant sur le fichier assets/datas.json

Le projet doit être opérationelle en production.

## Source

La source de la webapp est disponible ici:

```
/webapp
```

### Leaflet Documentation

[Leaflet Documentation](https://leafletjs.com/).

### Rainviewer Documentation

[Rainviewer Documentation](https://www.rainviewer.com/api.html).